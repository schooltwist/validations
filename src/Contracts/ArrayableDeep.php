<?php
namespace SchoolTwist\Validations\Contracts;
// TODO: Merge these - or something else more common
//namespace Illuminate\Contracts\Support;

interface ArrayableDeep
{
    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toDeepArray() : array;
}