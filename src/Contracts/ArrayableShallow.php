<?php
namespace SchoolTwist\Validations\Contracts;
// TODO: Merge these - or something else more common
//namespace Illuminate\Contracts\Support;

interface ArrayableShallow
{
    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toShallowArray() : array;
}