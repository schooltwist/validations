<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;


final class QuickMake_Test_Test extends TestCase
{


    function testQuickMake() {
        $dtv =\SchoolTwist\Validations\Returns\DtoValid::MakeAsValid();
        $this->assertTrue($dtv->isValid == true, "Should  get this far. ".__LINE__);


        $obj =\SchoolTwist\Validations\Returns\DtoValid::MakeAsInvalid();
        $this->assertTrue($obj->isValid == false, "Should  get this far. ".__LINE__);

    }


}